**DEPRECATED!** Replacement: [@csedl/stimulus-initializer](https://www.npmjs.com/package/@csedl/stimulus-initializer)

On Rails we are familiar with nice and handy conventions that saving a lot of time.

At [this tutorial](https://dev.to/chmich/setup-vite-svelte-inertia-stimulus-bootstrap-foundation-on-rails-7-overview-1bk1) i showed a way for integrating Svelte and Stimulus for handy tools for simplifying our javascript process.

On the [Handbook](https://stimulus.hotwired.dev/handbook/installing) Stimulus has shown a default way for declaring keys based of the controller filenames.  While this is a safe way i found this double-dashes silly and the namespaces could be shortened. Of course, if you want to have a naming where you can be 100% there is never a conflict, please stay at this default way. That is what this module, by default, does. 

For a smarter naming this module adds some options.

This module corresponds with [vite-svelte-initializer](https://gitlab.com/sedl/vite-svelte-initializer).

If, a conflict occours, it alerts on console with a log entry and on Browser with a alert. 

Assume a file structure like so:

`frontend/javascript/`

```
- components
   |-- articles
      |-- select-controller.js
- global
   |-- select-controller.js
```

## default: the safe way

`frontend/entrypoints/application.js`

```
import { initStimulus } from "vite-stimulus-initializer";

const comps = import.meta.glob('../javascript/components/**/*-controller.js', { eager: true })
initStimulus(comps, 2)
```

=> "2", the second parameter tells that the first two parts of the part (".." and "javascript" are excluded from namespacing, so it generates the tagname:

`components--articles--select`, which can be applied as custom element in the view by `<div data-controller="components--articles--select" > .. </div>`, and a `global--select` tag.

## optional: the smarter way

for making this more handy, you can:

`frontend/entrypoints/application.js`

```
import { initStimulus } from "vite-stimulus-initializer";

const apps = import.meta.glob('../javascript/components/**/*-controller.js', { eager: true })
initStimulus(apps, 2, { debug: true, exclude: ['components'], folderSeparator: '-' })
```

this prints out on console (because of `debug: true`):

- `STIMULUS IDENTIFIER «articles-select» from: ./components/views/admin/sub/next-controller.js`
- `STIMULUS IDENTIFIER «global-select» from: ./global/select-controller.js`

and having no more double-dash because of `folderSeparator: '-'`

The `exclude: [..]` is only applied for the level-3, which means: the first two levels are excluded completely because of second parameter, and the next is targeted by `exclude`.

and generates the corresponding tags.

Hope you enjoy!
